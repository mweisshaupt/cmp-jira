local cmp = require('cmp')
local curl = require "plenary.curl"
local utils = require('cmp_jira.utils')

local source = {
    config = {},
    filetypes = {},
    cache = {},
}

source.new = function(overrides)
    local self = setmetatable({}, {
        __index = source,
    })

    self.config = vim.tbl_extend("force", require("cmp_jira.config"), overrides or {})
    for _, item in ipairs(self.config.filetypes) do
        self.filetypes[item] = true
    end

    -- defaults
    if self.config.jira.jql == nil or self.config.jira.jql == "" then
        self.config.jira.jql = "assignee=%s+and+resolution=unresolved"
    end

    return self
end

function source:is_available()
        return self.filetypes["*"] ~= nil or self.filetypes[vim.bo.filetype] ~= nil
end

function source:complete(_, callback)
    -- try to get the items from cache first before calling the API
    local bufnr = vim.api.nvim_get_current_buf()
    if self.cache[bufnr] then
        callback({ items = self.cache[bufnr] })
        return true
    end

    local req_url = utils.get_request_url(self.config)

    local function curl_callback(out)
        local ok, parsed_issues = utils.parse_api_response(out.body)
        if not ok then
            return false
        end

        local items = {}
        for _, i in ipairs(parsed_issues) do
            table.insert(items, {
                label = i.key .. ": ".. i.summary,
                documentation = {
                    kind = "markdown",
                    value = i.description,
                },
                insertText = i.key,
                word = i.key,
            })
        end

        -- update the cache
        self.cache[bufnr] = items

        callback({ items = items })
        return true
    end

    -- run curl command
    if self.basicauth then
        local basic_auth = utils.get_basic_auth(self.config)
        curl.get(req_url, {
            auth = basic_auth,
            callback = curl_callback
        })
    else
        local bearer_token = utils.get_bearer_auth()
        curl.get(req_url, {
            headers = {
                Authorization = bearer_token,
            },
            callback = curl_callback
        })
    end
    return false
end

function source:get_debug_name()
    return "cmp_jira"
end

return source
