local M = {
    filetypes = { 'gitcommit' },
    basicauth = true,
    jira = {
        url = '',
        email = '',
        jql = 'assignee=%s+and+resolution=unresolved',
    },
}

return M
